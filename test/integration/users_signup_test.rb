require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

	test "Shouldnt create user with invalid signup info" do
		get signup_path
		assert_no_difference 'User.count' do
			post users_path, user: { name: "",
				email: "in@valid",
				password: "doesnt",
				password_confirmation: "match" }
		end

		# check if template is rerendered
		assert_template 'users/new'
	end

	test "Create user when valid information submitted" do
		get signup_path
		assert_difference 'User.count', 1 do
			post_via_redirect users_path, user: { name: "Lachlan",
				email: "valid_email@outlook.com", 
				password: "valid_password", 
				password_confirmation: "valid_password" }
		end
		assert_template 'users/show'
	end
end
