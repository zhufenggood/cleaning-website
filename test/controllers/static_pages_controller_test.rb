require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Cleaning Site"
  end

  test "should get contact" do
    get :help
    assert_response :success
    assert_select "title", "Contact | Cleaning Site"
  end

  test "should get about" do
  	get :about
  	assert_response :success
    assert_select "title", "About | Cleaning Site"
  end

end
