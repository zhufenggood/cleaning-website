require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
	def setup
		@user = User.new(name: "Example", 
			email: "email@example.com",
			password: "password123",
			password_confirmation: "password123")
	end

	test "user should be valid" do
		assert @user.valid?
	end

	test "name should be present" do
		@user.name = "    "
		assert_not @user.valid?
	end

	test "email should not be null" do
		@user.email = "   "
		assert_not @user.valid?
	end

	test "name should not be too long" do
		@user.name = 'a' * 50
		assert_not @user.valid?
	end

	test "email should not be too long" do
		@user.email = 'a' * 240 + "@example.com"
		assert_not @user.valid?
	end

	test "should accept valid emails" do
		valid_addresses = %w[user@email.com user@EmAiL.CoM u_s_e_r@my.company.email blahblah@google.jp]
		valid_addresses.each do |valid_address|
			@user.email = valid_address
			assert @user.valid?, "#{valid_address.inspect} should be valid"
		end
	end

	test "should reject invalid emails" do 
		invalid_addresses = %w[blah@email,com derp_email.com user@exMAMPLE.]
		invalid_addresses.each do |invalid_address|
			@user.email = invalid_address
			assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
		end
	end

	test "duplicate emails should not be allowed" do
		dup_user = @user.dup
		dup_user.email = @user.email.upcase
		@user.save
		assert_not dup_user.valid?
	end

	test "email should be downcase" do
		@user.email = "EMAIL@EMAIL.COM"
		assert_no_match /[a-z]/, @user.email, "Capital letters not downcased"
		@user.email = @user.email.downcase
		assert_match /[a-z]/, @user.email, "Email was downcased"
	end

	test "password should not be blank" do
		@user.password = @user.password_confirmation = " " * 10
		assert_not @user.valid?
	end

	test "password should have a minimum length" do
		@user.password = @user.password_confirmation = "a" * 5
		assert_not @user.valid?
	end



end
