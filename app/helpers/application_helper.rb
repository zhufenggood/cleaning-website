module ApplicationHelper

	# return full title page per basis
	def full_title(page_title = '')
		base_title = "Cleaning Site"
		if page_title.empty?
			base_title
		else
			page_title + " | " + base_title
		end
	end
end
