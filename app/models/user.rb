class User < ActiveRecord::Base
	#has_secure_password
	# before save callback to downcse all emails
	# before saving to the database.
	before_save { self.email = email.downcase }
	
	# validate name
	validates :name, presence: true, length: { maximum: 49 }
	
	# validate email
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 250 },
				format: { with: VALID_EMAIL_REGEX },
				uniqueness: { case_sensitive: false }

	# validate password
	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }



end
